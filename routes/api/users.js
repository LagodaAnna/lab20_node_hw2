const express = require('express');
const { auth, ctrlWrapper } = require('../../middlewares');
const { users: ctrl } = require('../../controllers');

const router = express.Router();

router.get('/me', auth, ctrlWrapper(ctrl.getUser));
router.patch('/me', auth, ctrlWrapper(ctrl.changePassword));
router.delete('/me', auth, ctrlWrapper(ctrl.deleteUser));

module.exports = router;
