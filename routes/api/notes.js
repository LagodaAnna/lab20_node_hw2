const express = require('express');
const { auth, ctrlWrapper } = require('../../middlewares');
const { notes: ctrl } = require('../../controllers');

const router = express.Router();

router.get('/', auth, ctrlWrapper(ctrl.getAll));

router.post('/', auth, ctrlWrapper(ctrl.add));

router.get('/:id', auth, ctrlWrapper(ctrl.getById));

router.put('/:id', auth, ctrlWrapper(ctrl.updateById));

router.patch('/:id', auth, ctrlWrapper(ctrl.updateCompletedById));

router.delete('/:id', auth, ctrlWrapper(ctrl.deleteByIid));

module.exports = router;
