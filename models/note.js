const { Schema, model } = require('mongoose');

const noteSchema = Schema(
  {
    userId: {
      type: Schema.Types.ObjectId,
      ref: 'user',
      required: true
    },
    completed: {
      type: Boolean,
      default: false
    },
    text: {
      type: String,
      required: [true, 'Set text for note']
    },
    createdDate: {
      type: String,
      default: new Date().toLocaleString()
    }
  },
  {
    versionKey: false
  }
);

const Note = model('note', noteSchema);

module.exports = Note;
