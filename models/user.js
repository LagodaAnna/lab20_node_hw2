const { Schema, model } = require('mongoose');

const userSchema = Schema(
  {
    username: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    createdDate: {
      type: String,
      default: new Date().toLocaleString()
    }
  },
  {
    versionKey: false
  }
);

const User = model('user', userSchema);

module.exports = User;
