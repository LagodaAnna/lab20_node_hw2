import { showUserInfo } from './showProfile';

const authList = document.querySelector('.authorization-buttons-list');

export function createProfileBth() {
  const token = localStorage.getItem('token');
  if (token) {
    const profileListItem = document.createElement('li');
    const profileBtn = document.createElement('button');
    profileBtn.textContent = 'Profile';
    profileBtn.setAttribute('type', 'button');
    profileBtn.classList.add('profile-btn');
    profileListItem.append(profileBtn);
    authList.append(profileListItem);
    showUserInfo();
  }
}
