import { Notify } from 'notiflix/build/notiflix-notify-aio';

export function deleteUser() {
  const deleteUserBtn = document.querySelector('.deleteUserBtn');
  deleteUserBtn.addEventListener('click', onDeleteUserBtnClick);

  function onDeleteUserBtnClick() {
    fetchDeleteUser()
      .then((res) => res.json())
      .then((res) => {
        if (res.message !== 'Success') {
          Notify.failure(res.message);
          throw new Error();
        }
        Notify.success(res.message);
        clearAndReturnToStartPage();
      })
      .catch((error) => console.log(error));
  }

  function fetchDeleteUser() {
    const token = localStorage.getItem('token');
    const options = {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${JSON.parse(token)}`,
        'Content-Type': 'application/json; charset=UTF-8'
      }
    };

    return fetch('http://localhost:8080/api/users/me', options);
  }
}

function clearAndReturnToStartPage() {
  localStorage.removeItem('token');
  location.reload();
}
