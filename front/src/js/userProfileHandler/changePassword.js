import { Notify } from 'notiflix/build/notiflix-notify-aio';
import { changeUserPasswordFormTpl } from '../../templates/changePasswordForm';

export function changeUserPassword() {
  const changePassBtn = document.querySelector('.changePasswordBtn');
  changePassBtn.addEventListener('click', onChangePassBtnClick);

  function onChangePassBtnClick() {
    const userProfile = document.querySelector('.userProfile');
    userProfile.insertAdjacentHTML('beforeend', changeUserPasswordFormTpl());
    const form = document.querySelector('.changePasswordForm');
    form.addEventListener('submit', onSubmitPassword);
    changePassBtn.setAttribute('disabled', true);
  }

  function onSubmitPassword(evt) {
    evt.preventDefault();
    const form = evt.target;
    const { oldPass, newPass } = form.elements;
    if (newPass.value.trim() === '') {
      Notify.failure('New password is required');
      return;
    }
    fetchChangePassword(oldPass.value, newPass.value)
      .then((res) => res.json())
      .then((res) => {
        if (res.message !== 'Success') {
          Notify.failure(res.message);
          throw new Error();
        }
        Notify.success(res.message);
        form.remove();
        changePassBtn.removeAttribute('disabled');
      })
      .catch((error) => console.log(error));
  }

  function fetchChangePassword(oldPassword, newPassword) {
    const token = localStorage.getItem('token');
    const body = {
      oldPassword,
      newPassword
    };
    const options = {
      method: 'PATCH',
      body: JSON.stringify(body),
      headers: {
        Authorization: `Bearer ${JSON.parse(token)}`,
        'Content-Type': 'application/json; charset=UTF-8'
      }
    };

    return fetch('http://localhost:8080/api/users/me', options);
  }
}
