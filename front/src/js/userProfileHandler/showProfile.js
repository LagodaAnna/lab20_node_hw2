import { userProfileTpl } from '../../templates/userProfile';
import { changeUserPassword } from './changePassword';
import { deleteUser } from './deleteUser';
const notesContainer = document.querySelector('.notes-container');
const addTodoBtn = document.querySelector('.addTodo');
const addTodoFormContainer = document.querySelector('.addTodo-form-container');

export function showUserInfo() {
  const profileBtn = document.querySelector('.profile-btn');
  profileBtn.addEventListener('click', onProfileBtnClick);
}

function onProfileBtnClick() {
  fetchUserInfo()
    .then((res) => res.json())
    .then((res) => {
      const { _id, username, createdDate } = res.user;
      notesContainer.innerHTML = userProfileTpl(_id, username, createdDate);
      addTodoBtn.setAttribute('disabled', true);
      addTodoFormContainer.innerHTML = '';
      changeUserPassword();
      deleteUser();
    })
    .catch((err) => console.log(err));
}

function fetchUserInfo() {
  const token = localStorage.getItem('token');
  return fetch('http://localhost:8080/api/users/me', {
    headers: {
      Authorization: `Bearer ${JSON.parse(token)}`
    }
  });
}
