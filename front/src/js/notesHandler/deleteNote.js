import { getAllNotes } from './getAll';

export function onDeleteBtnClick(evt) {
  if (evt.target.classList.contains('delete-btn')) {
    const { id } = evt.target.parentNode;
    fetchDeleteNote(id).then(() => getAllNotes());
  }
}

function fetchDeleteNote(id) {
  const token = localStorage.getItem('token');
  const options = {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${JSON.parse(token)}`
    }
  };

  return fetch(`http://localhost:8080/api/notes/${id}`, options);
}
