export function changeCompleted(evt) {
  if (evt.target.name === 'completed') {
    const noteBox = evt.target.parentNode;
    const { id } = noteBox;
    fetchCompletedNote(id);
  }
}

function fetchCompletedNote(id) {
  const token = localStorage.getItem('token');

  const options = {
    method: 'PATCH',
    headers: {
      Authorization: `Bearer ${JSON.parse(token)}`,
      'Content-Type': 'application/json; charset=UTF-8'
    }
  };

  return fetch(`http://localhost:8080/api/notes/${id}`, options);
}
