import { todoTpl } from '../../templates/todo';
const body = document.querySelector('.notes-container');
const addTodoBtn = document.querySelector('.addTodo');

export function getAllNotes() {
  page = 1;
  fetchAllNotes()
    .then((res) => res.json())
    .then((res) => {
      const listOfNotes = res.notes
        .map(({ _id, text, completed }) => {
          if (completed) {
            return todoTpl(_id, text, 'checked');
          }
          return todoTpl(_id, text);
        })
        .reverse()
        .join('');
      body.innerHTML = listOfNotes;
      addTodoBtn.removeAttribute('disabled');
    })
    .catch((error) => console.log(error));
}

function fetchAllNotes() {
  const token = localStorage.getItem('token');
  return fetch(`http://localhost:8080/api/notes/?page=${page}`, {
    headers: {
      Authorization: `Bearer ${JSON.parse(token)}`
    }
  });
}
