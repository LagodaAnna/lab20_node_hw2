import { Notify } from 'notiflix/build/notiflix-notify-aio';
import { addTodoFormTpl } from '../../templates/addTodoForm';
import { getAllNotes } from './getAll';
const formContainer = document.querySelector('.addTodo-form-container');

export function showTodoForm() {
  formContainer.innerHTML = addTodoFormTpl;
  const todoForm = document.querySelector('.todoForm');
  todoForm.addEventListener('submit', addNoteInList);
}

function addNoteInList(evt) {
  evt.preventDefault();
  const todoText = evt.target.elements.text.value;
  if (!todoText) {
    Notify.failure('Text is required');
    formContainer.innerHTML = '';
    return;
  }
  postNote(todoText)
    .then(() => {
      formContainer.innerHTML = '';
      getAllNotes();
    })
    .catch((error) => console.log(error));
}

function postNote(text) {
  const token = localStorage.getItem('token');

  const body = {
    text
  };

  const options = {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      Authorization: `Bearer ${JSON.parse(token)}`,
      'Content-Type': 'application/json; charset=UTF-8'
    }
  };

  return fetch('http://localhost:8080/api/notes/', options);
}
