import { getAllNotes } from './getAll';
import { changeNoteTpl } from '../../templates/changeNote';

export function onChangeBtnClick(evt) {
  if (evt.target.classList.contains('change-btn')) {
    const changeBtn = evt.target;
    const noteBox = evt.target.parentNode;
    const { id } = noteBox;
    const noteText = noteBox.firstElementChild;
    const changeForm = changeNoteTpl();
    noteText.insertAdjacentHTML('afterend', changeForm);
    const form = document.querySelector('.changeForm');
    changeBtn.setAttribute('disabled', true);
    form.addEventListener('submit', (evt) => {
      onChangeFormSubmit(evt, id);
      form.remove();
      changeBtn.removeAttribute('disabled');
    });
  }
}

function fetchChangeNote(id, text) {
  const token = localStorage.getItem('token');
  const body = {
    text
  };
  const options = {
    method: 'PUT',
    body: JSON.stringify(body),
    headers: {
      Authorization: `Bearer ${JSON.parse(token)}`,
      'Content-Type': 'application/json; charset=UTF-8'
    }
  };

  return fetch(`http://localhost:8080/api/notes/${id}`, options);
}

function onChangeFormSubmit(evt, id) {
  evt.preventDefault();
  const text = evt.target.elements.changeFormInput.value;
  fetchChangeNote(id, text).then(() => getAllNotes());
}
