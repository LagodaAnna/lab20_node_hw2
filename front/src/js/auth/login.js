import { formTpl } from '../../templates/userForm';
import { onFormSubmit } from './controllers';

const urlLogin = 'http://localhost:8080/api/auth/login';
const loginBtn = document.querySelector('.login-button');
const body = document.querySelector('.notes-container');
if (loginBtn.textContent === 'Login') {
  loginBtn.addEventListener('click', login);
} else {
  loginBtn.removeEventListener('click', login);
}

export function login() {
  body.innerHTML = formTpl;
  const form = document.querySelector('.form');
  form.classList.add('form-login');
  form.classList.remove('form-register');
  if (form.classList.contains('form-login')) {
    const title = document.createElement('p');
    title.textContent = 'Login';
    form.prepend(title);
  }
  form.addEventListener('submit', (evt) => onFormSubmit(evt, urlLogin));
}
