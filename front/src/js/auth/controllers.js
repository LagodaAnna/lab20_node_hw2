import { Notify } from 'notiflix/build/notiflix-notify-aio';
import { getAllNotes } from '../notesHandler/getAll';
import { createProfileBth } from '../userProfileHandler/profileBtn';
import { logout } from './logout';

const body = document.querySelector('.notes-container');
const getAllBtn = document.querySelector('.getAll');
const addTodoBtn = document.querySelector('.addTodo');

export function onFormSubmit(evt, url) {
  evt.preventDefault();
  const { password, username } = evt.target.elements;
  fetchPostUser(username.value, password.value, url)
    .then((response) => response.json())
    .then((res) => {
      if (res.message !== 'Success') {
        Notify.failure(res.message);
        throw new Error();
      }
      Notify.success(res.message);
      evt.target.reset();
      body.innerHTML = '';
      const { jwt_token } = res;
      if (jwt_token) {
        localStorage.setItem('token', JSON.stringify(jwt_token));
        getAllBtn.removeAttribute('disabled');
        addTodoBtn.removeAttribute('disabled');
        createProfileBth();
        getAllNotes();
        logout();
      }
    })
    .catch((error) => console.log(error));
}

function fetchPostUser(username, password, url) {
  const body = {
    username,
    password
  };

  const options = {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json; charset=UTF-8'
    }
  };

  return fetch(url, options);
}
