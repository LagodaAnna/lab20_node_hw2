import { formTpl } from '../../templates/userForm';
import { onFormSubmit } from './controllers';

const urlRegister = 'http://localhost:8080/api/auth/register';
const registrationBtn = document.querySelector('.registration-button');
const body = document.querySelector('.notes-container');

registrationBtn.addEventListener('click', register);

function register() {
  body.innerHTML = formTpl;
  const form = document.querySelector('.form');
  form.classList.add('form-register');
  form.classList.remove('form-login');
  if (form.classList.contains('form-register')) {
    const title = document.createElement('p');
    title.textContent = 'Registration';
    form.prepend(title);
  }
  form.addEventListener('submit', (evt) => onFormSubmit(evt, urlRegister));
}
