import { login } from './login';

export function logout() {
  const loginBtn = document.querySelector('.login-button');
  const token = localStorage.getItem('token');
  if (token) {
    loginBtn.textContent = 'Logout';
  }
  if (loginBtn.textContent === 'Logout') {
    loginBtn.addEventListener('click', onLogoutBtnClick);
  } else {
    loginBtn.removeEventListener('click', onLogoutBtnClick);
  }

  function onLogoutBtnClick() {
    const profileBtn = document.querySelector('.profile-btn');
    const getAllBtn = document.querySelector('.getAll');
    const addTodoBtn = document.querySelector('.addTodo');
    const body = document.querySelector('.notes-container');
    localStorage.removeItem('token');
    profileBtn?.remove();
    getAllBtn.setAttribute('disabled', true);
    addTodoBtn.setAttribute('disabled', true);
    body.innerHTML = '';
    loginBtn.textContent = 'Login';
    loginBtn.addEventListener('click', login);
    loginBtn.removeEventListener('click', onLogoutBtnClick);
  }
}
