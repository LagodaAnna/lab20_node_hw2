export const formTpl = `<form class="form">
      <label class="username-label">
        Username
        <input  class="username-input" type="text" name="username" required/>
      </label>

      <label class="password-label">
        Password
        <input class="password-input" type="password" name="password" required/>
      </label>

      <button type="submit" class="form-button">Submit</button>
    </form>`;
