export function changeUserPasswordFormTpl() {
  return `<form class="changePasswordForm">
        <label>Old password <input type="password" name="oldPass" required/></label>
        <label>New password <input type="password" name="newPass" required/></label>
        <button type="submit">Submit</button>
      </form>`;
}
