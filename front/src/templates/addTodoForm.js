export const addTodoFormTpl = `<form class="todoForm">
      <textarea name="text" cols="30" rows="10" placeholder="Add some text"></textarea>
      <button type="submit" class="todoForm-button">Submit</button>
    </form>`;
