export function todoTpl(id, text, checked = '') {
  return `<div class="todo" id=${id}>
        <p class="text">${text}</p>
        <input type="checkbox" name="completed" class="todo__checkbox" ${checked}/>
        <button type="button" class="change-btn">Change</button>
        <button type="button" class="delete-btn">Delete</button>
      </div>`;
}
