export function userProfileTpl(id, username, createdDate) {
  return `<div class="userProfile">
  <p class="userProfile__id">id: ${id}</p>
  <p class="userProfile__username">username: ${username}</p>
  <p class="userProfile__date">created date: ${createdDate}</p>
  <button class="changePasswordBtn" type="button">Change password</button>
   <button class="deleteUserBtn" type="button">DELETE USER</button>
  </div>`;
}
