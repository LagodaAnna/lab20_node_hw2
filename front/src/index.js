import './js/auth/registration';
import './js/auth/login';
import { getAllNotes } from './js/notesHandler/getAll';
import { showTodoForm } from './js/notesHandler/addNote';
import { onDeleteBtnClick } from './js/notesHandler/deleteNote';
import { onChangeBtnClick } from './js/notesHandler/changeNote';
import { createProfileBth } from './js/userProfileHandler/profileBtn';
import { changeCompleted } from './js/notesHandler/changeCompleted';
import { logout } from './js/auth/logout';

const getAllBtn = document.querySelector('.getAll');
const addTodoBtn = document.querySelector('.addTodo');
const notesContainer = document.querySelector('.notes-container');
const loginBtn = document.querySelector('.login-button');

const token = localStorage.getItem('token');
if (token) {
  loginBtn.textContent = 'Logout';
  logout();
  getAllBtn.removeAttribute('disabled');
  addTodoBtn.removeAttribute('disabled');
  createProfileBth();
   getAllNotes();
}

getAllBtn.addEventListener('click', getAllNotes);
addTodoBtn.addEventListener('click', showTodoForm);
notesContainer.addEventListener('click', onDeleteBtnClick);
notesContainer.addEventListener('click', onChangeBtnClick);
notesContainer.addEventListener('change', changeCompleted);