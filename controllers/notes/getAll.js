const { Note } = require('../../models');

const getAll = async (req, res) => {
  const { _id } = req.user;
  const { page = 1, limit = 100 } = req.query;
  const skip = (page - 1) * limit;
  const result = await Note.find({ userId: _id }, '', {
    skip,
    limit: Number(limit)
  });
  const count = await Note.find({ userId: _id }).count();
  res.json({
    offset: skip,
    limit,
    count,
    notes: result
  });
};

module.exports = getAll;
