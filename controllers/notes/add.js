const { Note } = require('../../models');

const add = async (req, res) => {
  const { _id } = req.user;
  await Note.create({ ...req.body, userId: _id });
  res.json({
    message: 'Success'
  });
};

module.exports = add;
