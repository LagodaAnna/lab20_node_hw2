const { Note } = require('../../models');
const { BadRequest } = require('http-errors');

const getById = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await Note.findById(id);
    if (!result) {
      throw new Error();
    }

    res.json({
      note: result
    });
  } catch (error) {
    throw new BadRequest(`Note with id ${id} not found`);
  }
};

module.exports = getById;
