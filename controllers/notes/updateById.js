const { Note } = require('../../models');
const { BadRequest } = require('http-errors');

const updateById = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await Note.findByIdAndUpdate(id, req.body);
    if (!result) {
      throw new Error();
    }
    res.json({
      message: 'Success'
    });
  } catch (error) {
    throw new BadRequest(`Note with id ${id} not found`);
  }
};

module.exports = updateById;
