const { Note } = require('../../models');
const { BadRequest } = require('http-errors');

const deleteByIid = async (req, res) => {
  const { id } = req.params;
  try {
    const note = await Note.findByIdAndDelete(id);
    if (!note) {
      throw new Error();
    }
    res.json({
      message: 'Success'
    });
  } catch (error) {
    throw new BadRequest(`Note with id ${id} not found`);
  }
};

module.exports = deleteByIid;
