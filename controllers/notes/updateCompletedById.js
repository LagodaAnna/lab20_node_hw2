const { Note } = require('../../models');
const { BadRequest } = require('http-errors');

const updateCompletedById = async (req, res) => {
  const { id } = req.params;
  try {
    const note = await Note.findById(id);
    if (!note) {
      throw new Error();
    }
    await Note.findByIdAndUpdate(id, { completed: !note.completed });
    res.json({
      message: 'Success'
    });
  } catch (error) {
    throw new BadRequest(`Note with id ${id} not found`);
  }
};

module.exports = updateCompletedById;
