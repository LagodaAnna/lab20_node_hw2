const add = require('./add');
const getAll = require('./getAll');
const getById = require('./getById');
const updateById = require('./updateById');
const updateCompletedById = require('./updateCompletedById');
const deleteByIid = require('./deleteById');

module.exports = {
  add,
  getAll,
  getById,
  updateById,
  updateCompletedById,
  deleteByIid
};
