const getUser = async (req, res) => {
  const { _id, username, createdDate } = req.user;

  res.json({
    user: {
      _id,
      username,
      createdDate
    }
  });
};

module.exports = getUser;
