const { User } = require('../../models');
const bcrypt = require('bcryptjs');
const { BadRequest } = require('http-errors');

const changePassword = async (req, res) => {
  const { _id, password } = req.user;
  const { oldPassword, newPassword } = req.body;
  const passCompare = bcrypt.compareSync(oldPassword, password);
  if (!passCompare) {
    throw new BadRequest('Wrong password');
  }

  const hashPassword = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(10));
  await User.findByIdAndUpdate(_id, { password: hashPassword });

  res.json({
    message: 'Success'
  });
};

module.exports = changePassword;
