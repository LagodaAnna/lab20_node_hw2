const getUser = require('./getUser');
const changePassword = require('./changePassword');
const deleteUser = require('./deleteUser');

module.exports = {
  getUser,
  changePassword,
  deleteUser
};
