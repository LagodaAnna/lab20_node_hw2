const { User } = require('../../models');

const deleteUser = async (req, res) => {
  const { _id } = req.user;

  await User.findByIdAndDelete(_id);

  res.json({
    message: 'Success'
  });
};

module.exports = deleteUser;
