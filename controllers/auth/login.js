const { User } = require('../../models');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { BadRequest } = require('http-errors');

const { SECRET_KEY } = process.env;

const login = async (req, res) => {
  const { username, password } = req.body;
  const user = await User.findOne({ username });
  if (!user) {
    throw new BadRequest(`User ${username} not found`);
  }

  const passCompare = bcrypt.compareSync(password, user.password);
  if (!passCompare) {
    throw new BadRequest('Wrong password');
  }

  const payload = {
    id: user._id
  };
  const token = jwt.sign(payload, SECRET_KEY);

  res.json({
    message: 'Success',
    jwt_token: token
  });
};

module.exports = login;
