const { User } = require('../../models');
const { Conflict } = require('http-errors');
const bcrypt = require('bcryptjs');

const register = async (req, res) => {
  const { username, password } = req.body;
  const user = await User.findOne({ username });
  if (user) {
    throw new Conflict(`User with username "${username}" has already exist`);
  }
  if (password.trim() === '') {
    throw new Conflict(`Password is required`);
  }
  const hashPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
  await User.create({ username, password: hashPassword });
  res.json({
    message: 'Success'
  });
};

module.exports = register;
