const notes = require('./notes');
const auth = require('./auth');
const users = require('./users');

module.exports = {
  notes,
  auth,
  users
};
