const mongoose = require('mongoose');
require('dotenv').config();
const express = require('express');
const cors = require('cors');
const logger = require('morgan');

const authRouter = require('./routes/api/auth');
const usersRouter = require('./routes/api/users');
const notesRouter = require('./routes/api/notes');

const { DB_HOST, PORT = 8080 } = process.env;
const app = express();

app.use(cors());
app.use(express.json());
app.use(logger('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);

app.use((req, res) => {
  res.status(400).json({ message: 'Bad Request' });
});

app.use((err, req, res, next) => {
  const { status = 500, message = 'Server error' } = err;
  res.status(status).json({ message });
});

mongoose
  .connect(DB_HOST)
  .then(() => {
    console.log('Database connect');
    app.listen(PORT);
  })
  .catch((err) => {
    console.log(`Error ${err.message}`);
    process.exit(1);
  });
